from django.shortcuts import render, redirect, HttpResponseRedirect
from .models import Jadwal
from .forms import JadwalForm
import datetime
from django.utils import timezone



# Create your views here.
def landing(request):
    return render(request, 'index.html')

def gallery(request):
    return render(request, 'gallery.html')

def tampil(request):
    form = JadwalForm()
    kegiatan = Jadwal.objects.all()
    context = {
        'item' : kegiatan,
        'form' : form
    }
    return render(request, 'form.html', context)

def create_jadwal(request):
    form = JadwalForm(request.POST)
    if form.is_valid():
        form.save()

    return redirect('homepage:form')

def delete(request, query_id):
    Jadwal.objects.filter(id=query_id).delete()
    return redirect('homepage:form')

