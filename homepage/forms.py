from django import forms
import datetime
from .models import Jadwal

class JadwalForm(forms.ModelForm):
    nama_kegiatan = forms.CharField(widget=forms.TextInput(attrs={
            "class" : "form-control",
            "required" : True,
            "placeholder":"Activity",
            }), label ="Nama Kegiatan")
    tempat = forms.CharField(widget=forms.TextInput(attrs={
            "class" : "form-control",
            "required" : True,
            "placeholder":"Place",
            }), label ="Place")
    kategori = forms.CharField(widget=forms.TextInput(attrs={
            "class" : "form-control",
            "required" : True,
            "placeholder":"Category",
            }), label ="Category")
    tanggal = forms.DateField(widget=forms.DateInput(attrs={
            "class" : "form-control",
            "required" : True,
            'placeholder':'Select a date',
            'type':'date',
            }), label ="Date")
    waktu = forms.TimeField(widget=forms.TimeInput(attrs={
            "class" : "form-control",
            "placeholder":"hh:mm",
            "required" : True,
            "type" : 'time',
            }), label ="Time") 

    class Meta:
        model = Jadwal
        fields = [
            'nama_kegiatan',
            'kategori',
            'tempat',
            'tanggal',
            'waktu',
        ]