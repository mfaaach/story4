from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.landing, name='landing'),
    path('gallery/', views.gallery, name='gallery'),
    path('form/', views.tampil , name='form'),
    path('buat/', views.create_jadwal, name='buat'),
    path('<int:query_id>', views.delete, name='value')
    # dilanjutkan ...
]